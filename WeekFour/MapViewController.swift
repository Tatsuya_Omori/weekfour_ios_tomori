import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var btn: UIButton!
    
    var departureAddress: String?
    var destinationAddress: String?
    
    var startLocation: String?
    var destination: String?
    
    var mapView: GMSMapView?
    
    var searchKey1: String?
    var searchKey2: String?
    
    let geocoder = CLGeocoder()
    
    let apiKey = "AIzaSyAZYSXbnpwaNYDrIVTqpNXwTBxSe0o9pI0"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchKey1 = String(departureAddress!)
        searchKey2 = String(destinationAddress!)
        
        GMSServices.provideAPIKey(apiKey)

        geocoder.geocodeAddressString(searchKey1!, completionHandler: { (placemarks, error) in
            if let unwrapPlacemarks = placemarks {
                if let firstPlacemark = unwrapPlacemarks.first {
                    if let location = firstPlacemark.location {
                        
                        let targetCoordinate = location.coordinate
                        self.startLocation = "\(location.coordinate.latitude),\(location.coordinate.longitude)"
                        
                        let camera = GMSCameraPosition.camera(withLatitude: targetCoordinate.latitude, longitude: targetCoordinate.longitude, zoom: 10.0)
                        
                        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
                        self.view = self.mapView
                        
                        self.mapView?.addSubview(self.btn)
                        self.view.sendSubviewToBack(self.mapView!)
                        
                        let marker1 = GMSMarker()
                        
                        marker1.position = CLLocationCoordinate2D(latitude: targetCoordinate.latitude, longitude: targetCoordinate.longitude)
                        marker1.title = self.searchKey1
                        marker1.map = self.mapView
                        
                        self.geocoder.geocodeAddressString(self.searchKey2!, completionHandler: { (placemarks, error) in
                            if let unwrapPlacemarks = placemarks {
                                if let firstPlacemark = unwrapPlacemarks.first {
                                    if let location = firstPlacemark.location {
                                        
                                        let targetCoordinate = location.coordinate
                                        self.destination = "\(location.coordinate.latitude),\(location.coordinate.longitude)"

                                        let marker2 = GMSMarker()

                                        marker2.position = CLLocationCoordinate2D(latitude: targetCoordinate.latitude, longitude: targetCoordinate.longitude)
                                        marker2.title = self.searchKey2
                                        marker2.map = self.mapView
                                        
                                        self.direction(destination: self.destination!, startLocation: self.startLocation!)
                                    }
                                }
                            }
                        })
                    }
                }
            }
        })
    }
    
    func direction(destination: String, startLocation: String) {
        let url: String = "https://maps.googleapis.com/maps/api/directions/json?origin=\(String(describing: startLocation))&destination=\(String(describing: destination))&mode=driving&key=\(apiKey)"
 
        AF.request(url).responseJSON { (reseponse) in
            guard let data = reseponse.data else {
                print("error")
                return
            }
            do {
                let jsonData = try JSON(data: data)
                let routes = jsonData["routes"].arrayValue
                
                for route in routes {
                    let overview_polyline = route["overview_polyline"].dictionary
                    let points = overview_polyline?["points"]?.string
                    let path = GMSPath.init(fromEncodedPath: points ?? "")
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeColor = .systemBlue
                    polyline.strokeWidth = 5
                    polyline.map = self.mapView
                }
            }
             catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
