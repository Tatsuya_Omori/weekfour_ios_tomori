import UIKit
import GooglePlaces

class ViewController: UIViewController, UITextFieldDelegate {

    let autocompleteController = GMSAutocompleteViewController()
    
    @IBOutlet weak var inputDepartureField: UITextField!
    @IBOutlet weak var inputDestinationField: UITextField!
    
    var departureData: String?
    var destinationData: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        inputDepartureField.delegate = self
        inputDestinationField.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
         
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMapViewController" {
            let next = segue.destination as? MapViewController
            next?.departureAddress = departureData
            next?.destinationAddress = destinationData
        }
    }

    @IBAction func inputPlace1(_ sender: UITextField) {
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func inputPlace2(_ sender: UITextField) {
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    @IBAction func didTapButton(_ sender: Any) {
        if inputDepartureField.text != "" && inputDestinationField.text != "" {
            self.performSegue(withIdentifier: "toMapViewController", sender: nil)
        }
    }
}

extension ViewController: GMSAutocompleteViewControllerDelegate {

    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if departureData == nil {
            inputDepartureField.text = place.name
            departureData = place.formattedAddress
            
                    } else {
            inputDestinationField.text = place.name
            destinationData = place.formattedAddress
        }

        print("Place name: \(String(describing: place.name))")
        print("Place address: \(String(describing: place.formattedAddress))")

        dismiss(animated: true, completion: nil)
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }

    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
}

